import ComponentName from './ComponentName';
import './App.css';

function App() {
  return (
    <div className="App">
      <ComponentName />
      <ComponentName />
    </div>
  );
}

export default App;
